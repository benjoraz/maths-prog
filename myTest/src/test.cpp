#include <gtest/gtest.h>
#include "Ratio.hpp"
const uint DEFAULT_MAX_VALUE = 20;

TEST (DefaultValue, zero) { 
	EXPECT_EQ(0, Ratio<int>().getNumerator());
	EXPECT_EQ(1, Ratio<int>().getDenominator());
}

TEST(TemplateType, Integer){
	Ratio<int> value(3,4);
	EXPECT_TRUE(typeid(int) == typeid(value.getNumerator()));
}

TEST(TemplateType, LongInt){
	Ratio<long int> value(3,5);
	EXPECT_TRUE(typeid(long int) == typeid(value.getNumerator()));
}

TEST(TemplateType, NegativeDenominator){
	Ratio<int> value(2,-3);
	EXPECT_FALSE(value.getDenominator() < 0);
	EXPECT_EQ(-2, value.getNumerator());
	EXPECT_EQ(3,value.getDenominator());
}

TEST(TemplateType, SignCorrection){
	Ratio<int> value(-2,-3);
	EXPECT_FALSE(value.getDenominator() < 0);
	EXPECT_EQ(2, value.getNumerator());
	EXPECT_EQ(3, value.getDenominator());
}

TEST(Constructor, ClassicExplicit){
	Ratio<int> value(2,3);
	EXPECT_EQ(2, value.getNumerator());
	EXPECT_EQ(3, value.getDenominator());
}

TEST(Constructor, Integer){
	Ratio<int> value(5);
	EXPECT_EQ(5, value.getNumerator());
	EXPECT_EQ(1, value.getDenominator());
}

TEST(Constructor, Float){
	Ratio<int> value(2.5);
	EXPECT_EQ(5, value.getNumerator());
	EXPECT_EQ(2, value.getDenominator());
}

TEST(Simplify, Constructor){
	Ratio<int> value(10,5);
	EXPECT_EQ(2, value.getNumerator());
	EXPECT_EQ(1, value.getDenominator());
}

TEST(Operators, Addition){
	Ratio<int> value(1,2);
	Ratio<int> value2(3,4);
	Ratio<int> valueAdd(value+value2);
	EXPECT_EQ(5, valueAdd.getNumerator());
	EXPECT_EQ(4, valueAdd.getDenominator());
}

TEST(Simplify, Addition){
	Ratio<int> value(6,7);
	Ratio<int> value2(9,14);
	Ratio<int> valueAdd(value+value2);
	EXPECT_EQ(3, valueAdd.getNumerator());
	EXPECT_EQ(2, valueAdd.getDenominator());
}

TEST(Operators, Substraction){
	Ratio<int> value(3,4);
	Ratio<int> value2(1,2);
	Ratio<int> valueSub(value - value2);
	EXPECT_EQ(1, valueSub.getNumerator());
	EXPECT_EQ(4, valueSub.getDenominator());
}

TEST(Simplify, Substraction){
	Ratio<int> value(5,4);
	Ratio<int> value2(3,4);
	Ratio<int> valueSub(value - value2);
	EXPECT_EQ(1, valueSub.getNumerator());
	EXPECT_EQ(2, valueSub.getDenominator());	
}

TEST(Operators, Multiply){
	Ratio<int> value(1,2);
	Ratio<int> value2(3,4);
	Ratio<int> valueTimes(value*value2);
	EXPECT_EQ(3, valueTimes.getNumerator());
	EXPECT_EQ(8, valueTimes.getDenominator());
}

TEST(Simplify, Multiply){
	Ratio<int> value(1,2);
	Ratio<int> value2(4,5);
	Ratio<int> valueTimes(value*value2);
	EXPECT_EQ(2, valueTimes.getNumerator());
	EXPECT_EQ(5, valueTimes.getDenominator());
}

TEST(Operators, MultiplyScalar){
	Ratio<int> value(1,2);
	double mult = 3.0;
	Ratio<int> valueTimes(value * mult);
	EXPECT_EQ(3, valueTimes.getNumerator());
	EXPECT_EQ(2, valueTimes.getDenominator());
}

TEST(Simplify, MultiplyScalar){
	Ratio<int> value(1,6);
	double mult = 9.0;
	Ratio<int> valueTimes(value * mult);
	EXPECT_EQ(3, valueTimes.getNumerator());
	EXPECT_EQ(2, valueTimes.getDenominator());
}

TEST(Operators, Division){
	Ratio<int> value(3,7);
	Ratio<int> value2(2,9);
	Ratio<int> valueDiv(value/value2);
	EXPECT_EQ(27, valueDiv.getNumerator());
	EXPECT_EQ(14, valueDiv.getDenominator());
}

TEST(Simplify, Division){
	Ratio<int> value(4,7);
	Ratio<int> value2(2,9);
	Ratio<int> valueDiv(value/value2);	
	EXPECT_EQ(18, valueDiv.getNumerator());
	EXPECT_EQ(7, valueDiv.getDenominator());
}

TEST(StandardOperators, Absolute){
	Ratio<int> value(-10, 7);
	Ratio<int> valueAbs(Ratio<int>::abs(value));
	EXPECT_EQ(10, valueAbs.getNumerator());
	EXPECT_EQ(7, valueAbs.getDenominator());
}

TEST(Operators, UnaryMinus){
	Ratio<int> value(7,2);
	Ratio<int> value2 = -value;
	EXPECT_EQ(-7,value2.getNumerator());
	EXPECT_EQ(2, value2.getDenominator());
}

TEST(StandardOperators, Inverse){
	Ratio<int> value(2, 17);
	Ratio<int> value2(value.reverseNumDenum());
	EXPECT_EQ(value.getNumerator(), value2.getDenominator());
	EXPECT_EQ(value.getDenominator(), value2.getNumerator());
}

TEST(StandardOperators, NegativeInverse){
	Ratio<int> value(-2, 17);
	Ratio<int> value2(value.reverseNumDenum());
	EXPECT_EQ(value.getNumerator(), -value2.getDenominator());
	EXPECT_EQ(value.getDenominator(), -value2.getNumerator());
}

TEST(ComparisonOperators, IsLowerTrue){
	Ratio<int> value(3,4);
	Ratio<int> value2(4,5);
	EXPECT_TRUE(value < value2);
	EXPECT_TRUE(Ratio<int>(7) < Ratio<int>(8));
}

TEST(ComparisonOperators, IsLowerFalse){
	Ratio<int> value(3,4);
	Ratio<int> value2(4,5);
	EXPECT_FALSE(value2 < value);
	EXPECT_FALSE(Ratio<int>(8) < Ratio<int>(7));	
}

TEST(ComparisonOperators, IsLowerEqualTrue){
	Ratio<int> value(2,3);
	Ratio<int> value2(4,6);
	EXPECT_TRUE(value <= value2);
	EXPECT_TRUE(value2 <= value);
	EXPECT_TRUE(Ratio<int>(1,2) <= Ratio<int>(7,8));
	EXPECT_TRUE(Ratio<int>(1) <= Ratio<int>(3));
}

TEST(ComparisonOperators, IsLowerEqualFalse){
	EXPECT_FALSE(Ratio<int>(7,2) <= Ratio<int>(7,8));
	EXPECT_FALSE(Ratio<int>(7) <= Ratio<int>(3));	
}

TEST(ComparisonOperators, isEqualFalse){
	Ratio<int> value(1,2);
	Ratio<int> value2(2,3);
	EXPECT_FALSE(value == value2);
}

TEST(ComparisonOperators, isEqualTrue){
	Ratio<int> value(1,2);
	Ratio<int> value2(2,4);
	EXPECT_TRUE(value == value2);
}

TEST(ComparisonOperators, isDifferentFalse){
	Ratio<int> value(1,2);
	Ratio<int> value2(2,4);
	EXPECT_FALSE(value != value2);	
}

TEST(ComparisonOperators, isDifferentTrue){
	Ratio<int> value(1,2);
	Ratio<int> value2(2,3);
	EXPECT_TRUE(value != value2);
}

TEST(ComparisonOperators, isGreaterFalse){
	Ratio<int> value(1,7);
	Ratio<int> value2(8,11);
	EXPECT_FALSE(value > value2);
	EXPECT_FALSE(Ratio<int>(3) > Ratio<int>(57));
}

TEST(ComparisonOperators, isGreaterTrue){
	Ratio<int> value(8,7);
	Ratio<int> value2(8,11);
	EXPECT_TRUE(value > value2);
	EXPECT_TRUE(Ratio<int>(800) > Ratio<int>(57));
}

TEST(ComparisonOperators, isGreaterEqualFalse){
	Ratio<int> value(1,8);
	Ratio<int> value2(8,3);
	EXPECT_FALSE(value >= value2);
	EXPECT_FALSE(Ratio<int>(3) >= Ratio<int>(8));
}

TEST(ComparisonOperators, isGreaterEqualTrue){
	Ratio<int> value(2,3);
	Ratio<int> value2(4,6);
	EXPECT_TRUE(value >= value2);
	EXPECT_TRUE(Ratio<int>(3.5) >= Ratio<int>(3));
}

TEST(FriendOperators, Print){
	std::ostringstream oula;
	oula << Ratio<int>(5) << std::endl;
	EXPECT_EQ("5/1\n",oula.str());
}

TEST(Exception, ZeroOutOfZero){
	EXPECT_THROW(Ratio<int>(0,0), std::domain_error);
}
TEST(Exception, ImpossibleCalcul){
	EXPECT_THROW(Ratio<int>(0,1)*Ratio<int>(1,0), std::domain_error);
}

TEST(Simplify, toZero){
	Ratio<int> value(0,11);
	EXPECT_EQ(0, value.getNumerator());
    EXPECT_EQ(1, value.getDenominator());
}

TEST(Simplify, ToInfinity){
	Ratio<int> value(11,0);
	EXPECT_EQ(1, value.getNumerator());
    EXPECT_EQ(0, value.getDenominator());
}

TEST(Simplify, ToNegativeInfinity){
	Ratio<int> value(-11,0);
	EXPECT_EQ(-1, value.getNumerator());
	EXPECT_EQ(0, value.getDenominator());
}

TEST(Operands, Sin){
	Ratio<int> value(4,2);
	EXPECT_EQ(std::sin(2), Ratio<int>::sin(value));
}

TEST(Operands, Cos){
	Ratio<int> value(4,2);
	EXPECT_EQ(std::sin(2), Ratio<int>::sin(value));
}

TEST(Operands, Tan){
	Ratio<int> value(4,2);
	EXPECT_EQ(std::tan(2), Ratio<int>::tan(value));
}

TEST(Operands, Abs){
	Ratio<int> value(-4,2);
	Ratio<int> valueAbs = Ratio<int>::abs(value);
	EXPECT_EQ(4, valueAbs.getNumerator());	
	EXPECT_EQ(2, valueAbs.getDenominator());
}

TEST(Operands, Sqrt){
	Ratio<int> value(7,2);
	Ratio<int> ValueSqrt = Ratio<int>::sqrt(value);
	EXPECT_EQ((std::sqrt(7)/std::sqrt(2)),Ratio<int>::sqrt(value));
}