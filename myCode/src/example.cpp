#include <iostream>
#include "Ratio.hpp"

int main(){
    std::cout << "\nBonjour et bienvenue sur la petite présentation (en Francais) de la librairie Ratio made by moi-même \n\n\nVoici quelques exemples pour pouvoir comprendre comment marche cette petite beauté : \n\n" << std::endl;
    std::cout << "Pour commencer, vous pouvez déclarer votre nombre Rationnel avec 'Ratio<T> name()', sachant que T peut-être tout type entier, comme des int, long int ou long long int." << std::endl;
    std::cout << "Si vous voulez déclarer un Rationnel de int vous pouvez écrire 'Ratio<int> ratio()', les exemples suivants vous montreront comment construire un ratio et comment l'utiliser :\n" << std::endl;

    Ratio<int> ratio1 = Ratio<int>();
    Ratio<int> ratio2(ratio1);
    Ratio<int> ratio3(2,3);
    Ratio<int> ratio4(-3.5);

    std::cout << "(Ratio<int> ratio1 = Ratio<int>()) => " << ratio1 << '\n' << "Ratio<int> ratio2(ratio1) = " << ratio2 << '\n' << "Ratio<int> ratio3(2,3) = " << ratio3 << '\n' << "Ratio<int> ratio4(3.5) = " << ratio4 << '\n' << std::endl;
    std::cout << "Et évidemment ça serait nul si on pouvait pas faire des calculs, alors toute une panoplie d'opérateurs est à votre disposition ;)\n" << std::endl;

    Ratio<int> ratio5 = ratio3 + ratio4;
    Ratio<int> ratio6 = ratio3 * ratio4;
    Ratio<int> ratio7 = ratio3 - ratio4;
    Ratio<int> ratio8 = ratio3/ratio4;

    std::cout << "(Ratio<int> ratio5 = ratio3 + ratio4) => " << ratio5 << '\n' << "(Ratio<int> ratio6 = ratio3 * ratio4) => " << ratio6 << '\n' << "(Ratio<int> ratio7 = ratio3 - ratio4) => " << ratio7 << '\n' << "(Ratio<int> ratio8 = ratio3/ratio4) => " << ratio8 << '\n' << std::endl;
    std::cout << "Il y a aussi possibilité de faire ces calculs avec des nombres arithmétiques, ainsi que d'utiliser le moins unaire. \n" << std::endl;

    Ratio<int> ratio9 = ratio3 + 6.5;
    Ratio<int> ratio10 = ratio3 * 3.0;
    Ratio<int> ratio11 = ratio3 - 1.5;
    Ratio<int> ratio12 = ratio3/3;
    Ratio<int> ratio13 = -ratio3;

    std::cout << "(Ratio<int> ratio9 = ratio3 + 6.5) => " << ratio9 << "\n(Ratio<int> ratio10 = ratio3 * 3.0) => " << ratio10 << "\n(Ratio<int> ratio11 = ratio3 - 1.5) => " << ratio11 << "\n(Ratio<int> ratio12 = ratio3/3) => " << ratio12 << "\n(Ratio<int> ratio13 = -ratio3) => " << ratio13 << '\n' << std::endl;
    std::cout << "Et évidemment ça marche dans l'autre sens : \n" << std::endl;

    float ratio14 = 6.5 + ratio3;
    float ratio15 = 3.0 * ratio3;
    float ratio16 = 1.5 - ratio3;
    float ratio17 = 3.0/ratio3;   

    std::cout << "(float ratio14 = 6.5 + ratio3) => " << ratio14 << "\n(float ratio15 = 3.0 * ratio3) => " << ratio15 << "\n(float ratio16 = 1.5 - ratio3) => " << ratio16 << "\n(float ratio17 = 3/ratio3) => " << ratio17 << '\n' << std::endl;
    std::cout << "Vous pouvez aussi utiliser les opérateurs de comparaison : \n" << std::endl;
    
    bool ratio18 = (ratio1 == ratio2);
    bool ratio19 = (ratio3 > ratio2);
    bool ratio20 = (ratio2 < ratio3);
    bool ratio21 = (ratio3 >= ratio2);
    bool ratio22 = (ratio2 <= ratio3);
    bool ratio23 = (ratio3 != ratio1);

    std::cout << "(bool ratio18 = (ratio1 == ratio2)) ===> " << ratio18 << "\n(bool ratio19 = (ratio3 > ratio2)) ===> " << ratio19 << "\n(bool ratio20 = (ratio2 < ratio3)) ===> " << ratio20 << "\n(bool ratio21 = (ratio3 >= ratio2)) ===> " << ratio21 << "\n(bool ratio22 = (ratio2 <= ratio3)) ===> " << ratio22 << "\n(bool ratio23 = (ratio3 != ratio1)) ===> " << ratio23 << '\n' << std::endl;
    std::cout << "Et finalement vous pouvez aussi utiliser les opérateurs algébriques suivants : \n" << std::endl;

    double ratio24 = Ratio<int>::sin(ratio3);
    double ratio25 = Ratio<int>::cos(ratio3);
    double ratio26 = Ratio<int>::tan(ratio3);
    double ratio27 = Ratio<int>::sqrt(ratio3);
    Ratio<int> ratio28 = Ratio<int>::abs(ratio4);

    std::cout << "(double ratio24 = Ratio<int>::sin(ratio3)) ===> " << ratio24 << "\n(double ratio25 = Ratio<int>::cos(ratio3)) ===> " << ratio25 << "\n(double ratio26 = Ratio<int>::tan(ratio3)) ===> " << ratio26 << "\n(double ratio27 = Ratio<int>::sqrt(ratio3)) ===> " << ratio27 << "\n(Ratio<int> ratio28 = Ratio<int>::abs(ratio3)) ===> " << ratio28 << "\nVous pouvez converetir directement ces doubles en rajoutant ToRatio après l'opérateur, comme par exemple avec sinToRatio().\n\nVoilà c'était tout pour aujourd'hui bonne nuit à vous :)" << std::endl;

    return 0;
}