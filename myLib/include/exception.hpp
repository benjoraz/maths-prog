#include <iostream>

#pragma once

struct ImpossibleRatio : public std::exception{
    const char * what() const throw()
    {
        return "You created a 0/0 ratio";
    }
};
