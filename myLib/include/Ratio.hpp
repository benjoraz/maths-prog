#include <iostream>
#include <cmath>
#include <typeinfo>
#include <assert.h>
#include <numeric>
#include <cstdlib>
#include "exception.hpp"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#pragma once

// Doxygen menu
/// \version 0.1
/// \mainpage
/// \image html myImage.jpg
/// \tableofcontents
/// \section instroduction_sec What for?
/// VectorD is a super tool.
/// \section install_bigsec How to install
/// Yeah Ratio is a class using rationnal numbers as a new type :)
/// \subsection dependencies_sec Dependecies
/// \li STL
/// \li Doxygen (if you want the documentation)
/// \subsection install_sec Install with cmake (Linux / Mac)
/// \li go to main dir
/// \li mkdir build
/// \li cd build
/// \li cmake ..
/// \li make
/// \li if Doxygen installed: make html
/// \li The documentation is located in :
/// 	- [path to build]/doc/doc-doxygen/html/index.html or 
/// 	- or [path to build]/INTERFACE/doc/doc-doxygen/html/index.html
/// \li You can find a demo at /myCode/srx/example.cpp
/// \section credits_sec Credits
/// \li Made by juste Benjamin Razafimanantsoa seul :(


/// \class Ratio
/// \brief class defining a Rational number for linear algebra operations
template <typename T>
class Ratio{

    private :
        T m_numerator; /**<Rational numerator*/
        T m_denominator; /**<Rationnal denominator*/

        void setSignOnNumerator();
        constexpr inline void checkType(){
            static_assert(std::is_integral<T>(), "The members of a Ratio class cannot be a floating-point type");
        };
        constexpr void checkPossible();
        constexpr void testPossible();
        void reduceWithPgcd();
        template<typename H>
        constexpr Ratio<T> floatToRatio(H toConvert, int nbIter){
            static_assert(std::is_integral<T>(), "The function 'floatToRatio' must return a Ratio class with an integral members");
            if(toConvert == 0){
                return Ratio<T>(0,1);
            }
            if(nbIter == 0){
                return Ratio<T>(0,1);
            }
            if(toConvert < 1 && toConvert > -1){
                return Ratio<T>(floatToRatio<H>(1/toConvert,nbIter)).reverseNumDenum();
            }
            else{
                H wholePart = static_cast<T>(toConvert);
                return Ratio<T>(wholePart,1) + floatToRatio<H>((toConvert - wholePart), (nbIter - 1));
            }
        }
    
    public :

        /// \brief Default constructor
        constexpr Ratio();
        /// \brief Constructor from a copy
        /// \param Ratio<T> : will copy all of his members 
        constexpr Ratio(const Ratio<T> &copied);
        /// \brief Constructor from a numerator and a denominator
        /// \param newNumerator : has to be an integer
        /// \param newDenominator : has too be an integer
        constexpr Ratio(const T &newNumerator, const T &newDenominator);
        /// \brief Constructor from a value
        /// \param value : is converted to a Ratio
        template <typename H>
        constexpr Ratio(const H &value);
        
        /// \brief setter for numerator
        constexpr inline T & setNumerator() {return m_numerator;}
        /// \brief setter for denominator
        constexpr inline T & setDenominator() {return m_denominator;}
        /// \brief getter for numerator
        constexpr inline const T & getNumerator() const {return m_numerator;}
        /// \brief getter for denominator
        constexpr inline const T & getDenominator() const {return m_denominator;}

        /// \brief function to convert a ratio into a float
        /// \param Ratio<T> : uses his numerator and denominator to do the conversion
        /// \return float
        constexpr inline friend float ratioToFloat(const Ratio<T> &aRatio) {return float(aRatio.getNumerator())/(aRatio.getDenominator());}
        /// \brief method to check if a ratio is infinite
        inline bool checkInf() {if(m_denominator == 0){return true;}return false;}
        /// \brief method to reverse the numerator and the denominator
        Ratio<T> reverseNumDenum();
        /// \brief method to convert a ratio into a float
        float ratioToFloat() const;

        /// \brief Multiplication of 2 ratios (can be use in the other order)
        /// \param Ratio
        /// \return Ratio
        Ratio<T> operator*(const Ratio &otherRatio) const;
        /// \brief Additiob of 2 ratios (can be use in the other order)
        /// \param Ratio
        /// \return Ratio
        Ratio<T> operator+(const Ratio &otherRatio) const;
        /// \brief Substraction of 2 ratios (can be use in the other order)
        /// \param Ratio
        /// \return Ratio
        Ratio<T> operator-(const Ratio &otherRatio) const;
        /// \brief Division of 2 ratios (can be use in the other order)
        /// \param Ratio
        /// \return Ratio
        Ratio<T> operator/(const Ratio &otherRatio) const;
        /// \brief Multiplication of a ratio by a value (can be use in the other order)
        /// \param Arithmetic
        /// \return Ratio
        template <typename H>
        Ratio<T> operator*(const H &value) const;
        /// \brief Addition of a ratio with a value (can be use in the other order)
        /// \param Arithmetic
        /// \return Ratio
        template <typename H>
        Ratio<T> operator+(const H &value) const;
        /// \brief Substraction of a ratio with a value (can be use in the other order)
        /// \param Arithmetic
        /// \return Ratio
        template <typename H>
        Ratio<T> operator-(const H &value) const;
        /// \brief Division of a ratio by a value (can be use in the other order)
        /// \param Arithmetic
        /// \return Ratio
        template <typename H>
        Ratio<T> operator/(const H &value) const;
        /// \brief Define the value of a ratio from a value (can be use in the other order)
        /// \param Arithmetic
        /// \return Ratio
        template <typename H>
        Ratio<T> operator=(const H &value) const;

        inline friend Ratio<T> operator-(const Ratio<T> &aRatio){return Ratio<T>(-1*aRatio.getNumerator(), aRatio.getDenominator());}
        template <typename H>
        inline friend H operator*(const H &value, const Ratio<T> &aRatio){return (static_cast<H>(aRatio.getNumerator())/aRatio.getDenominator()) * value;}
        template <typename H>
        inline friend H operator+(const H &value, const Ratio<T> &aRatio){return (static_cast<H>(aRatio.getNumerator())/aRatio.getDenominator()) + value;}
        template <typename H>
        inline friend H operator-(const H &value, const Ratio<T> &aRatio){return (static_cast<H>(-(aRatio.getNumerator())/aRatio.getDenominator())) + value;}
        template <typename H>
        inline friend H operator/(const H &value, const Ratio<T> &aRatio){
            Ratio<T> theValue(aRatio);
            Ratio<T> newValue(theValue.reverseNumDenum());
            return value * newValue;
        }

        /// \brief comparison operator 'isEqual'
        /// \param Ratio
        /// \return bool
        bool operator==(const Ratio<T> &aRatio);
        /// \brief comparison operator 'isDifferent'
        /// \param Ratio
        /// \return bool
        bool operator!=(const Ratio<T> &aRatio);
        /// \brief comparison operator 'isLower'
        /// \param Ratio
        /// \return bool
        bool operator<(const Ratio<T> &aRatio);
        /// \brief comparison operator 'isGreater'
        /// \param Ratio
        /// \return bool
        bool operator>(const Ratio<T> &aRatio);
        /// \brief comparison operator 'isLowerEqual'
        /// \param Ratio
        /// \return bool
        bool operator<=(const Ratio<T> &aRatio);
        /// \brief comparison operator 'isGreaterEqual'
        /// \param Ratio
        /// \return bool
        bool operator>=(const Ratio<T> &aRatio);

        /// \brief get the absolute value of a ratio
        /// \param Ratio
        /// \return Ratio
        static inline Ratio<T> abs(const Ratio<T> &aRatio){return Ratio<T>(std::abs(aRatio.getNumerator()), aRatio.getDenominator());}
        /// \brief get the int part of a Ratio
        /// \param Ratio
        /// \return int
        static int intPart(const Ratio<T> &aRatio);
        /// \brief get the value of a Ratio powered to a number (write powToRatio() to convert it directly in a ratio)
        /// \param Ratio
        /// \param Arithmetic
        /// \return double
        template <typename H>
        static double pow(const Ratio<T> &aRatio, const H &power);
        /// \brief get the value of a number powered to a Ratio (write powToRatio() to convert it directly in a ratio)
        /// \param Arithmetic
        /// \param Ratio
        /// \return double
        template <typename H>
        static double pow(const H &power, const Ratio<T> &aRatio);
        template <typename H>
        static Ratio<T> powToRatio(const H &power, const Ratio<T> &aRatio);
        static Ratio<T> powToRatio(const Ratio<T> &aRatio, const int &power);
        template <typename H>
        static Ratio<T> powToRatio(const Ratio<T> &aRatio,const H &power);
        /// \brief get the cos value of a ratio (write cosToRatio() to convert it directly in a ratio)
        /// \param Ratio
        /// \return double
        static double cos(const Ratio<T> &aRatio);
        static Ratio<T> cosToRatio(const Ratio<T> &aRatio);
        /// \brief get the sin value of a ratio (write sinToRatio() to convert it directly in a ratio)
        /// \param Ratio
        /// \return double
        static double sin(const Ratio<T> &aRatio);
        static Ratio<T> sinToRatio(const Ratio<T> &aRatio);
        /// \brief get the tan value of a ratio (write tanToRatio() to convert it directly in a ratio)
        /// \param Ratio
        /// \return double
        static double tan(const Ratio<T> &aRatio);
        static Ratio<T> tanToRatio(const Ratio<T> &aRatio);
        /// \brief get the sqrt value of a ratio (write sqrtToRatio() to convert it directly in a ratio)
        /// \param Ratio
        /// \return double
        static double sqrt(const Ratio<T> &aRatio);
        static Ratio<T> sqrtToRatio(const Ratio<T> &aRatio);

        /// \brief you can use a std::cout to show a Ratio
        template <typename H>
        friend std::ostream& operator<<(std::ostream& stream, const Ratio<H> &ratio);
};

template <typename T>
std::ostream& operator<<(std::ostream& stream, const Ratio<T> &ratio){
    if(ratio.getDenominator() == 0){
        if(ratio.getNumerator() > 0){
            stream << "infinity";}
        else if(ratio.getNumerator() < 0){
            stream << "-infinity";}
        else{
            stream << "0/0 mais c'est pas censé arriver";}
    }
    else{
        stream << ratio.getNumerator() << "/" << ratio.getDenominator();}
    return stream;
}

template <typename T>
constexpr Ratio<T>::Ratio() : m_numerator(static_cast<T>(0)), m_denominator(static_cast<T>(1)) {checkType();}

template <typename T>
constexpr Ratio<T>::Ratio(const Ratio<T> &copied) : m_numerator(copied.m_numerator), m_denominator(copied.m_denominator){}

template <typename T>
constexpr Ratio<T>::Ratio(const T &newNumerator, const T &newDenominator) : m_numerator(newNumerator), m_denominator(newDenominator){
    testPossible();
    checkType();
    reduceWithPgcd();
    setSignOnNumerator();
}

template <typename T> 
template <typename H>
constexpr Ratio<T>::Ratio(const H &value){
    floatToRatio(value,20);
    m_numerator = floatToRatio(value,20).getNumerator();
    m_denominator = floatToRatio(value,20).getDenominator();        
    testPossible();
    checkType();
    reduceWithPgcd();
    setSignOnNumerator();
}

template <typename T>
Ratio<T> Ratio<T>::operator*(const Ratio<T> &otherRatio) const{
    Ratio<T> newValue(m_numerator * otherRatio.m_numerator, m_denominator * otherRatio.m_denominator);
    newValue.testPossible();
    newValue.reduceWithPgcd();
    return newValue;
}

template <typename T>
Ratio<T> Ratio<T>::operator+(const Ratio<T> &otherRatio) const{
    T newNum = (m_numerator * otherRatio.m_denominator) + (m_denominator * otherRatio.m_numerator);
    T newDeno = (m_denominator * otherRatio.m_denominator);
    Ratio<T> newValue(newNum, newDeno);
    newValue.checkPossible();
    newValue.reduceWithPgcd();
    return newValue;
}

template <typename T>
Ratio<T> Ratio<T>::operator-(const Ratio<T> &otherRatio) const{
    T newNum = (m_numerator * otherRatio.m_denominator) - (m_denominator * otherRatio.m_numerator);
    T newDeno = (m_denominator * otherRatio.m_denominator);
    Ratio<T> newValue(newNum, newDeno);
    newValue.checkPossible();
    newValue.reduceWithPgcd();
    return newValue;
}

template <typename T>
Ratio<T> Ratio<T>::operator/(const Ratio<T> &otherRatio) const{
    Ratio<T> test(otherRatio);
    Ratio<T> newRatio(test.reverseNumDenum());
    Ratio<T> newValue((*this) * newRatio);
    newValue.checkPossible();
    newValue.reduceWithPgcd();
    newValue.setSignOnNumerator();
    return newValue;
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::operator*(const H &value) const{
    Ratio<T> newValue(value);
    Ratio<T> xValue(*this * newValue);
    xValue.checkPossible();
    xValue.reduceWithPgcd();
    xValue.setSignOnNumerator();
    return xValue;
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::operator+(const H &value) const{
    Ratio<T> newValue(value);
    Ratio<T> xValue(*this + newValue);
    xValue.checkPossible();
    xValue.reduceWithPgcd();
    return xValue;
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::operator-(const H &value) const{
    Ratio<T> newValue(value);
    Ratio<T> xValue(*this - newValue);
    xValue.checkPossible();
    xValue.reduceWithPgcd();
    return xValue;
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::operator/(const H &value) const{
    Ratio<T> newValue(value);
    Ratio<T> xValue(*this/newValue);
    xValue.checkPossible();
    xValue.reduceWithPgcd();
    xValue.setSignOnNumerator();
    return xValue;
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::operator=(const H &value) const{
    Ratio<T> newValue(value);
    m_numerator = newValue.getNumerator();
    m_denominator = newValue.getDenominator();
}

template <typename T>
bool Ratio<T>::operator==(const Ratio<T> &aRatio){
    if(getDenominator() != 0 && aRatio.getDenominator()){
        float a = ratioToFloat();
        float b = aRatio.ratioToFloat();
        if(a == b){
            return true;
        }
        return false;
    }
    else if(getDenominator() == 0 && aRatio.getDenominator() == 0){
        return true;
    }
    return false;
}

template <typename T>
bool Ratio<T>::operator!=(const Ratio<T> &aRatio){
    float a = ratioToFloat();
    float b = aRatio.ratioToFloat();
    if(a != b){
        return true;
    }
    return false;
}

template <typename T>
bool Ratio<T>::operator<(const Ratio<T> &aRatio){
    float a = ratioToFloat();
    float b = aRatio.ratioToFloat();
    if(a < b){
        return true;
    }
    return false;
}

template <typename T>
bool Ratio<T>::operator>(const Ratio<T> &aRatio){
    float a = ratioToFloat();
    float b = aRatio.ratioToFloat();
    if(a > b){
        return true;
    }
    return false;
}

template <typename T>
bool Ratio<T>::operator<=(const Ratio<T> &aRatio){
    float a = ratioToFloat();
    float b = aRatio.ratioToFloat();
    if(a <= b){
        return true;
    }
    return false;
}

template <typename T>
bool Ratio<T>::operator>=(const Ratio<T> &aRatio){
    float a = ratioToFloat();
    float b = aRatio.ratioToFloat();
    if(a >= b){
        return true;
    }
    return false;
}

template <typename T>
int Ratio<T>::intPart(const Ratio<T> &aRatio){
    if(aRatio.getDenominator() == 0){
        return std::numeric_limits<int>::max();
    }
    return int(aRatio.m_numerator/aRatio.m_denominator);
}

template <typename T>
template <typename H>
double Ratio<T>::pow(const Ratio<T> &aRatio,const H &power){
    return std::pow(aRatio.ratioToFloat(),power);
}

template <typename T>
template <typename H>
double Ratio<T>::pow(const H &number, const Ratio<T> &aRatio){
    return std::pow(number,aRatio.ratioToFloat());
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::powToRatio(const H &number, const Ratio<T> &aRatio){
    return Ratio<T>(Ratio<T>::pow(number, aRatio));
}

template <typename T>
Ratio<T> Ratio<T>::powToRatio(const Ratio<T> &aRatio,const int &power){
    if(power >= 0){
        return Ratio<T>(pow(aRatio.m_numerator, power), pow(aRatio.m_denominator, power));
    }
    else{
        return Ratio<T>(pow(aRatio.m_denominator, -1*power), pow(aRatio.m_numerator, -1*power));
    }
}

template <typename T>
template <typename H>
Ratio<T> Ratio<T>::powToRatio(const Ratio<T> &aRatio,const H &power){
    return Ratio<T>(Ratio<T>::pow(aRatio));
}

template <typename T>
double Ratio<T>::cos(const Ratio<T> &aRatio){
    return std::cos(aRatio.ratioToFloat());
}

template <typename T>
Ratio<T> Ratio<T>::cosToRatio(const Ratio<T> &aRatio){
    return Ratio<T>(Ratio<T>::cos(aRatio));
}

template <typename T>
double Ratio<T>::sin(const Ratio<T> &aRatio){
    return std::sin(aRatio.ratioToFloat());
}

template <typename T>
Ratio<T> Ratio<T>::sinToRatio(const Ratio<T> &aRatio){
    return Ratio<T>(Ratio<T>::sin(aRatio));
}

template <typename T>
double Ratio<T>::tan(const Ratio<T> &aRatio){
    return std::tan(aRatio.ratioToFloat());
}

template <typename T>
Ratio<T> Ratio<T>::tanToRatio(const Ratio<T> &aRatio){
    return Ratio<T>(Ratio<T>::tan(aRatio));
}

template <typename T>
double Ratio<T>::sqrt(const Ratio<T> &aRatio){
    return std::sqrt(aRatio.ratioToFloat());
}

template <typename T>
Ratio<T> Ratio<T>::sqrtToRatio(const Ratio<T> &aRatio){
    return Ratio<T>(Ratio<T>::sqrt(aRatio));
}

template <typename T>
constexpr void Ratio<T>::checkPossible(){
    if((m_numerator == 0) && (m_denominator == 0)){
        throw ImpossibleRatio();
    }
}

template <typename T>
constexpr void Ratio<T>::testPossible(){
    try {
        checkPossible();
    }
    catch (ImpossibleRatio &e) {
        std::cout << "Caught an exception : " << '\n' << e.what() << std::endl;
        exit(1);
    }    
}

template <typename T>
Ratio<T> Ratio<T>::reverseNumDenum(){
    Ratio<T> newValue;
    newValue.m_numerator = m_denominator;
    newValue.m_denominator = m_numerator;
    newValue.reduceWithPgcd();
    newValue.setSignOnNumerator();
    return newValue;
}

template <typename T>
void Ratio<T>::setSignOnNumerator(){
    if(m_denominator < 0){
        m_numerator *= -1;
        m_denominator *= -1;
    }
}

template <typename T>
void Ratio<T>::reduceWithPgcd(){
    T thePgcd = std::abs(std::gcd(m_numerator, m_denominator));
    m_numerator = m_numerator/thePgcd;
    m_denominator = m_denominator/thePgcd;
}

template <typename T>
float Ratio<T>::ratioToFloat() const{
    if(getDenominator() == 0){
        return std::numeric_limits<float>::infinity();}
        return float(m_numerator)/(m_denominator);  
}
